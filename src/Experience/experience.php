<?php

namespace App\Experience;
use PDO;
class experience
{
    private $id='';
    private $postid='';
    private $mainid='';
    private $designation='';
    private $company_name='';
    private $start_date='';
    private $end_date='';
    private $company_location='';
    private $pdo='';

    public function __construct()
    {
        session_start();
        $this->pdo= new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
    }
    public function setData($data=''){
        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }
        if(array_key_exists('postid',$data)){
            $this->postid=$data['postid'];
        }
        if(array_key_exists('designation',$data)){
            $this->designation=$data['designation'];
        }
        if(array_key_exists('company_name',$data)){
            $this->company_name=$data['company_name'];
        }
        if(array_key_exists('start_date',$data)){
            $this->start_date=$data['start_date'];
        }
        if(array_key_exists('end_date',$data)){
            $this->end_date=$data['end_date'];
        }
        if(array_key_exists('company_location',$data)){
            $this->company_location=$data['company_location'];
        }
        if(array_key_exists('mainid',$data)){
            $this->mainid=$data['mainid'];
        }
        return $this;
    }//End of setData

    public function store(){
        try {
            $query = "INSERT INTO `experiences` (`id`, `user_id`, `designation`, `company_name`,`start_date`,`end_date`,`company_location`)
                      VALUES (:id,:uid,:designation,:company_name,:start_date,:end_date,:company_location)";
//            echo $query;
//            die();
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute(
                array(
                    ':id' => null,
                    ':uid' => $this->id,
                    ':designation' => $this->designation,
                    ':company_name' => $this->company_name,
                    ':start_date' => $this->start_date,
                    ':end_date' => $this->end_date,
                    ':company_location' => $this->company_location,
                )
            );
            if ($stmnt) {
                $_SESSION['experience-message'] = "Successfully Data Stored";
                header("location:add_experience.php?id=$this->id");
            }
        } catch (PDOException $e) {
            echo 'Error' . $e->getMessage();
        }
    }//END OF STORE
    public function show(){
        try{
            $query = "SELECT * FROM `experiences` WHERE `user_id` = "."'".$this->id."'";
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute();
            $value = $stmnt->fetchAll();
            return $value;
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }//End of show

    public function experienceshow(){
        try{
            $query = "SELECT * FROM `experiences` WHERE `id` = "."'".$this->id."'";
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute();
            $value = $stmnt->fetch();
            return $value;
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }//End of show
    public function update()
    {
        try {
            $query = 'UPDATE experiences SET `id`=:id,`user_id`=:uid,`designation`=:designation,`company_name`=:company_name,`start_date`=:start_date,
                      `end_date`=:end_date,`company_location`=:company_location WHERE id = :id';
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute(
                array(
                    ':id' => $this->postid,
                    ':uid' => $this->id,
                    ':designation' => $this->designation,
                    ':company_name' => $this->company_name,
                    ':start_date' => $this->start_date,
                    ':end_date' => $this->end_date,
                    ':company_location' => $this->company_location,
                )
            );
            if ($stmnt) {
                $_SESSION['update-mess'] = "Successfully Data Updated";
                header("location:update_experience.php?id=$this->postid");
            }
        }
        catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }//End of update
    public function delete()
    {
        try {
            $query = "DELETE FROM `experiences` WHERE `experiences`.`id` =". $this->id;
            $stmnt = $this->pdo->query($query);
            $stmnt->execute();
            if ($stmnt) {
                $_SESSION['message'] = "Successfully Data Deleted";
                header("location:../Experience/experience_view.php?id=$this->mainid");
            }
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }//End of Delete
}