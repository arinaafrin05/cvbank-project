<?php

namespace App\Awards;
use PDO;
class award
{
    private $id = "";
    private $postid = "";
    private $mainid = "";
    private $title = "";
    private $organization = "";
    private $description = "";
    private $location = "";
    private $year = "";
    private $pdo = "";

    public function __construct()
    {
        session_start();
        $this->pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');

    }

    public function setData($data = '')
    {
        if (array_key_exists('title', $data)) {
            $this->title = $data['title'];
        }
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('organization', $data)) {
            $this->organization = $data['organization'];
        }
        if (array_key_exists('description', $data)) {
            $this->description = $data['description'];
        }
        if (array_key_exists('location', $data)) {
            $this->location = $data['location'];
        }
        if (array_key_exists('year', $data)) {
            $this->year = $data['year'];
        }
        if (array_key_exists('postid', $data)) {
            $this->postid = $data['postid'];
        }
        if (array_key_exists('mainid', $data)) {
            $this->mainid = $data['mainid'];
        }
        return $this;
    }//End of setData

    public function store()
    {
        try {
            $query = "INSERT INTO `awards` (`id`, `user_id`, `title`, `organization`, `description`,`location`,`year`)
                      VALUES (:id,:uid,:title,:organization,:description,:location,:year)";
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute(
                array(
                    ':id' => null,
                    ':uid' => $this->id,
                    ':title' => $this->title,
                    ':organization' => $this->organization,
                    ':description' => $this->description,
                    ':location' => $this->location,
                    ':year' => $this->year,
                )
            );
            if ($stmnt) {
                $_SESSION['award-message'] = "Successfully Data Stored";
                header("location:add_award.php?id=$this->id");
            }
        } catch (PDOException $e) {
            echo 'Error' . $e->getMessage();
        }


    }//End of store
    public function show(){
        try{
            $query = "SELECT * FROM `awards` WHERE `user_id` = "."'".$this->id."'";
//            echo $query;
//            die();
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute();
            $value = $stmnt->fetchAll();
            return $value;
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }//End of show
    public function awardshow(){
        try{
            $query = "SELECT * FROM `awards` WHERE `id` = "."'".$this->id."'";
//            echo $query;
//            die();
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute();
            $value = $stmnt->fetch();
            return $value;
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }//End of show
    public function update()
    {
        try {
            $query = 'UPDATE awards SET `id`=:id,`user_id`=:uid,`title`=:title,`organization`=:organization,`description`=:description,
                      `location`=:location, `year`=:year WHERE id = :id';
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute(
                array(
                    ':id' => $this->postid,
                    ':uid' => $this->id,
                    ':title' => $this->title,
                    ':organization' => $this->organization,
                    ':description' => $this->description,
                    ':location' => $this->location,
                    ':year' => $this->year,
                )
            );
            if ($stmnt) {
                $_SESSION['update-message'] = "Successfully Data Updated";
                header("location:update_award.php?id=$this->postid");
            }
        }
        catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }//End of update
    public function delete()
    {
        try {
            $query = "DELETE FROM `awards` WHERE `awards`.`id` =". $this->id;
            $stmnt = $this->pdo->query($query);
            $stmnt->execute();
            if ($stmnt) {
                $_SESSION['message'] = "Successfully Data Deleted";
                header("location:../Award/award_view.php?id=$this->mainid");
            }
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }//End of Delete
}