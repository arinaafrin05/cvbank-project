<?php

namespace App\education;
use PDO;
class education
{
    private $id = "";
    private $postid = "";
    private $mainid = "";
    private $title = "";
    private $institute = "";
    private $result = "";
    private $passing_year = "";
    private $main_subject = "";
    private $education_board = "";
    private $course_duration = "";
    private $pdo = "";

    public function __construct()
    {
        session_start();
        $this->pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');

    }

    public function setData($data = '')
    {
        if (array_key_exists('title', $data)) {
            $this->title = $data['title'];
        }
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('institute', $data)) {
            $this->institute = $data['institute'];
        }
        if (array_key_exists('result', $data)) {
            $this->result = $data['result'];
        }
        if (array_key_exists('postid', $data)) {
            $this->postid = $data['postid'];
        }
        if (array_key_exists('passing_year', $data)) {
            $this->passing_year = $data['passing_year'];
        }
        if (array_key_exists('main_subject', $data)) {
            $this->main_subject = $data['main_subject'];
        }
        if (array_key_exists('main_subject', $data)) {
            $this->main_subject = $data['main_subject'];
        }
        if (array_key_exists('education_board', $data)) {
            $this->education_board = $data['education_board'];
        }
        if (array_key_exists('course_duration', $data)) {
            $this->course_duration = $data['course_duration'];
        }
        if (array_key_exists('mainid', $data)) {
            $this->mainid = $data['mainid'];
        }
        return $this;
    }//End of setData

    public function store()
    {
        try {
            $query = "INSERT INTO `educations` (`id`, `user_id`, `title`, `institute`, `result`, `passing_year`, `main_subject`, `education_board`, `course_duration`)
                      VALUES (:id,:uid,:title,:institute,:result,:passing_year,:main_subject,:eduboard,:courseduration)";
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute(
                array(
                    ':id' => null,
                    ':uid' => $this->id,
                    ':title' => $this->title,
                    ':institute' => $this->institute,
                    ':result' => $this->result,
                    ':passing_year' => $this->passing_year,
                    ':main_subject' => $this->main_subject,
                    ':eduboard' => $this->education_board,
                    ':courseduration' => $this->course_duration
                )
            );
            if ($stmnt) {
                $_SESSION['message'] = "Successfully Data Stored";
                header("location:add_education.php?id=$this->id");
            }
        } catch (PDOException $e) {
            echo 'Error' . $e->getMessage();
        }


    }//End of store
    public function show(){
        try{
            $query = "SELECT * FROM `educations` WHERE `user_id` = "."'".$this->id."'";
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute();
            $value = $stmnt->fetchAll();
            return $value;
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }//End of show
    public function educationshow(){
        try{
            $query = "SELECT * FROM `educations` WHERE `id` = "."'".$this->id."'";
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute();
            $value = $stmnt->fetch();
            return $value;
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }//End of show
    public function update()
    {
        try {
            $query = 'UPDATE educations SET `id`=:id, `user_id`=:uid, `title`=:title, `institute`=:institute, `result`=:result, `passing_year`=:passing_year, `main_subject`=:main_subject, `education_board`=:eduboard, `course_duration`=:courseduration WHERE id = :id';
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute(
                array(
                    ':id' => $this->postid,
                    ':uid' => $this->id,
                    ':title' => $this->title,
                    ':institute' => $this->institute,
                    ':result' => $this->result,
                    ':passing_year' => $this->passing_year,
                    ':main_subject' => $this->main_subject,
                    ':eduboard' => $this->education_board,
                    ':courseduration' => $this->course_duration
                )
            );
            if ($stmnt) {
                $_SESSION['message'] = "Successfully Data Updated";
                header("location:update_education.php?id=$this->postid");
            }
        }
        catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }//End of update
    public function delete()
    {
        try {
            $query = "DELETE FROM `educations` WHERE `educations`.`id` =". $this->id;
            $stmnt = $this->pdo->query($query);
            $stmnt->execute();
            if ($stmnt) {
                $_SESSION['message'] = "Successfully Data Deleted";
                header("location:../Education/education_view.php?id=$this->mainid");
            }
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }//End of Delete
}