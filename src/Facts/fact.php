<?php

namespace App\Facts;
use PDO;
class fact
{
    private $id='';
    private $postid='';
    private $mainid='';
    private $title='';
    private $no_of_items='';
    private $img='';
    private $pdo='';

    public function __construct()
    {
        session_start();
        $this->pdo= new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');
    }

    public function setData($data=''){
        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }
        if(array_key_exists('postid',$data)){
            $this->postid=$data['postid'];
        }
        if (array_key_exists('img', $data)) {
            $this->img= $data['img'];
        }
        if(array_key_exists('title',$data)){
            $this->title=$data['title'];
        }
        if(array_key_exists('no_of_items',$data)){
            $this->no_of_items=$data['no_of_items'];
        }
        if(array_key_exists('mainid',$data)){
            $this->mainid=$data['mainid'];
        }
        return $this;
    }//End of setData

    public function store(){
        try {
            $query = "INSERT INTO `facts` (`id`, `user_id`, `title`, `no_of_items`,`img`)
                      VALUES (:id,:uid,:title,:items,:img)";
//            echo $query;
//            die();
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute(
                array(
                    ':id' => null,
                    ':uid' => $this->id,
                    ':title' => $this->title,
                    ':items' => $this->no_of_items,
                    ':img'=>$this->img,
                )
            );
            if ($stmnt) {
                $_SESSION['fact-message'] = "Successfully Data Stored";
                header("location:add_fact.php?id=$this->id");
            }
        } catch (PDOException $e) {
            echo 'Error' . $e->getMessage();
        }
    }//END OF STORE
    public function show(){
        try{
            $query = "SELECT * FROM `facts` WHERE `user_id` = "."'".$this->id."'";
//            echo $query;
//            die();
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute();
            $value = $stmnt->fetchAll();
            return $value;
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }//End of show

    public function factshow(){
        try{
            $query = "SELECT * FROM `facts` WHERE `id` = "."'".$this->id."'";
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute();
            $value = $stmnt->fetch();
            return $value;
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }//End of show

    public function factupdateshow(){
        try{
            $query = "SELECT * FROM `facts` WHERE `id` = "."'".$this->postid."'";
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute();
            $value = $stmnt->fetch();
            return $value;
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }//End of hobbiesshow
    public function update()
    {
        try {
//            echo $this->title.$this->no_of_items.$this->img.$this->id.$this->postid;
//            die();
            $query = 'UPDATE facts SET `id`=:id,`user_id`=:uid,`title`=:title,`no_of_items`=:items,`img`=:img WHERE id = :id';
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute(
                array(
                    ':id' => $this->postid,
                    ':uid' => $this->id,
                    ':title' => $this->title,
                    ':items' => $this->no_of_items,
                    ':img'=> $this->img,
                )
            );
            if ($stmnt) {
                $_SESSION['update-mess'] = "Successfully Data Updated";
                header("location:update_fact.php?id=$this->postid");
            }
        }
        catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }//End of update
    public function delete()
    {
        try {
            $query = "DELETE FROM `facts` WHERE `facts`.`id` =". $this->id;
            $stmnt = $this->pdo->query($query);
            $stmnt->execute();
            if ($stmnt) {
                $_SESSION['message'] = "Successfully Data Deleted";
                header("location:../Facts/fact_view.php?id=$this->mainid");
            }
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }//End of Delete

}