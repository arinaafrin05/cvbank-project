<?php

namespace App\id\about;
use PDO;
class about
{
    private $user_id = "";
    private $title = "";
    private $bio = "";
    private $phone = "";
    private $pdo = "";
    public function __construct()
    {
        session_start();
        $this->pdo = new PDO('mysql:host=localhost;dbname=cvbank','root','');

    }
    public function setData($data = '')
    {
        if (array_key_exists('title',$data)) {
            $this->title = $data['title'];
        }if (array_key_exists('user_id',$data)) {
        $this->user_id = $data['user_id'];
    }if (array_key_exists('bio',$data)) {
        $this->bio = $data['bio'];
    }if (array_key_exists('phone',$data)) {
        $this->phone = $data['phone'];
    }
        return $this;
    }//End of setData
    public function store()
    {
        try {
            $query = "INSERT INTO `abouts` (`id`, `user_id`, `title`, `phone`, `bio`)
                      VALUES (:id,:uid,:title,:phone,:bio)";
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute(
                array(
                    ':id' => null,
                    ':uid' => $this->user_id,
                    ':title' => $this->title,
                    ':phone' => $this->phone,
                    ':bio'=> $this->bio
                )
            );
            if ($stmnt) {
                $_SESSION['about-mas'] = "Successfully Data Stored.";
                header('location:about-create.php');
            }
        }catch (PDOException $e ) {
            echo 'Error' . $e->getMessage();
        }


    }//End of store
//    public function login(){
//        try{
//            $query = "SELECT * FROM `users` WHERE `username` = '$this->user_name' AND `password` = '$this->password'";
//            $stmnt = $this->pdo->prepare($query);
//            $stmnt->execute();
//            $value = $stmnt->fetch();
//            if (empty($value)) {
//                $_SESSION['fail'] = "Opps! Invalid email or password";
//                header('location:index.php');
//            } else {
//                $_SESSION['user_info'] = $value;
//                header('location:views/id/Mobile/dashboard.php');
//            }
//        }catch (PDOException $e) {
//            echo  'Error' . $e->getMessage();
//        }
//    }//End of login
//
//    public function checkusername($username){
//        try{
//            $query = "SELECT username FROM `users` WHERE `username` = '$username'";
//            $stmnt = $this->pdo->prepare($query);
//            $stmnt->execute();
//            $value = $stmnt->fetch();
//            if ($username == "") {
//                echo "<i class=\"icon-cancel-circle2 position-left\"></i> Please Enter Username";
//            } elseif($value) {
//                echo "<i class=\"icon-cancel-circle2 position-left\"></i> $username is already taken";
//            }
//            else{
//                echo "<i class=\"icon-checkmark position-left\"></i> $username is Available";
//            }
//            return $value;
//        }catch (PDOException $e) {
//            echo  'Error' . $e->getMessage();
//        }
//    }//End of checkusername
//    public function validusername(){
//        try{
//            $username = $this->user_name;
//            $password = $this->password;
//            $email = $this->email;
//            $query = "SELECT * FROM `users` WHERE `username` = '$username'";
//            $stmnt = $this->pdo->prepare($query);
//            $stmnt->execute();
//            $value = $stmnt->fetch();
//            if($value)
//            {
//                $_SESSION['message'] = "Username ($username) is already taken";
//                header('location:registration.php');
//            }
//            else{
//                $this->store($username,$password,$email);
//            }
//        }catch (PDOException $e) {
//            echo  'Error' . $e->getMessage();
//        }
//    }//End of login


//    public function index($perpage='',$offset=''){
//        try{
//            $query = "SELECT  SQL_CALC_FOUND_ROWS * FROM `sometable` WHERE `delected_at` = '0000-00-00 00:00:00'ORDER BY id DESC LIMIT $perpage OFFSET $offset";
//            $stmnt = $this->pdo->prepare($query);
//            $stmnt->execute();
//            $value = $stmnt->fetchAll();
//            $subquery = "SELECT FOUND_ROWS()";
//            $totalrow = $this->pdo->query($subquery)->fetch(PDO::FETCH_COLUMN);
//            $value['totalrow'] = $totalrow;
//            return $value;
//        }catch (PDOException $e) {
//            echo  'Error' . $e->getMessage();
//        }
//    }//End of index
//    public function show(){
//        try{
//            $query = "SELECT * FROM `sometable` WHERE unique_id="."'".$this->id."'";
//            $stmnt = $this->pdo->prepare($query);
//            $stmnt->execute();
//            $value = $stmnt->fetch();
//            return $value;
//        }catch (PDOException $e) {
//            echo  'Error' . $e->getMessage();
//        }
//    }//End of show
//    public function update()
//    {
//        try {
//            $query = 'UPDATE sometable SET title = :mobile_brand WHERE id = :id';
//            $stmnt = $this->pdo->prepare($query);
//            $stmnt->execute(
//                array(
//                    ':id' => $this->id,
//                    ':mobile_brand' => $this->title
//                )
//            );
//            if ($stmnt) {
//                $_SESSION['message'] = "Successfully Data Updated";
//                header('location:listbrand.php');
//            }
//        }
//        catch (PDOException $e) {
//            echo  'Error' . $e->getMessage();
//        }
//    }//End of update
//    public function trash()
//    {
//        try {
//            $query = 'UPDATE sometable SET delected_at = :delete_time WHERE id = :id';
//            $stmnt = $this->pdo->prepare($query);
//            $stmnt->execute(
//                array(
//                    ':id' => $this->id,
//                    ':delete_time' => date('Y-m-d h:m:s')
//                )
//            );
//            if ($stmnt) {
//                $_SESSION['message'] = "Successfully Data Deleted";
//                header('location:listbrand.php');
//            }
//        }
//        catch (PDOException $e) {
//            echo  'Error' . $e->getMessage();
//        }
//    }//End of Trash
//    public function trashted()
//    {
//        try{
//            $query = "SELECT * FROM `sometable` WHERE `delected_at` != '0000-00-00 00:00:00'";
//            $stmnt = $this->pdo->prepare($query);
//            $stmnt->execute();
//            $value = $stmnt->fetchAll();
//            return $value;
//        }catch (PDOException $e) {
//            echo  'Error' . $e->getMessage();
//        }
//    }//End of Trashted
//    public function restore()
//    {
//        try {
//            $query = 'UPDATE sometable SET delected_at = :delete_time WHERE id = :id';
//            $stmnt = $this->pdo->prepare($query);
//            $stmnt->execute(
//                array(
//                    ':id' => $this->id,
//                    ':delete_time' => '0000-00-00 00:00:00'
//                )
//            );
//            if ($stmnt) {
//                $_SESSION['message'] = "Successfully Data Restore";
//                header('location:trashlist.php');
//            }
//        }
//        catch (PDOException $e) {
//            echo  'Error' . $e->getMessage();
//        }
//    }//End of Trash
//    public function delete(){
//        try{
//            $pdo = new PDO('mysql:host=localhost;dbname=bitm','root','');
//            $query = "DELETE FROM `sometable` WHERE `sometable`.`id` =".$this->id;
//            $stmnt = $this->pdo->query($query);
//            $stmnt->execute();
//            if ($stmnt) {
//                $_SESSION['message'] = "Successfully Data Deleted";
//                header('location:trashlist.php');
//            }
//        }catch (PDOException $e) {
//            echo  'Error' . $e->getMessage();
//        }
//    }//End of delete


}
