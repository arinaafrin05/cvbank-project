<?php

namespace App\about;
use PDO;
class about
{
    private $id = "";
    private $postid = "";
    private $title = "";
    private $phone = "";
    private $bio = "";
    private $pdo = "";

    public function __construct()
    {
        session_start();
        $this->pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');

    }

    public function setData($data = '')
    {
        if (array_key_exists('title', $data)) {
            $this->title = $data['title'];
        }
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('phone', $data)) {
            $this->phone = $data['phone'];
        }
        if (array_key_exists('bio', $data)) {
            $this->bio = $data['bio'];
        }
        if (array_key_exists('postid', $data)) {
            $this->postid = $data['postid'];
        }
        return $this;
    }//End of setData

//    public function store()
//    {
//        try {
//            $query = "INSERT INTO `abouts` (`id`, `user_id`, `title`, `phone`, `bio`)
//                      VALUES (:id,:uid,:title,:phone,:bio)";
//            $stmnt = $this->pdo->prepare($query);
//            $stmnt->execute(
//                array(
//                    ':id' => null,
//                    ':uid' => $this->id,
//                    ':title' => $this->title,
//                    ':phone' => $this->phone,
//                    ':bio' => $this->bio
//                )
//            );
//            if ($stmnt) {
//                $_SESSION['about-message'] = "Successfully Data Stored";
//                header("location:add_about.php?id=$this->id");
//            }
//        } catch (PDOException $e) {
//            echo 'Error' . $e->getMessage();
//        }

//
//    }//End of store
    public function show(){
        try{
            $query = "SELECT * FROM `abouts` WHERE `user_id` = "."'".$this->id."'";
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute();
            $value = $stmnt->fetch();
            return $value;
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }//End of show
    public function update()
    {
        try {
//            echo $this->id.$this->title.$this->phone.$this->bio;
//            die();
            $query = 'UPDATE `abouts` SET `title`=:title,`phone`=:phone,`bio`=:bio WHERE user_id = :id';
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute(
                array(
                    ':id' => $this->id,
                    ':title' => $this->title,
                    ':phone' => $this->phone,
                    ':bio' => $this->bio,
                )
            );
            if ($stmnt) {
                $_SESSION['update-mess'] = "Successfully Data Updated";
                header("location:update_about.php?id=$this->id");
            }
        }
        catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }//End of update
}