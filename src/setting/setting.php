<?php
namespace App\setting;
use PDO;
class setting
{
    private $id = "";
    private $postid = "";
    private $title = "";
    private $fullname = "";
    private $description = "";
    private $address = "";
    private $img = "";
    private $pdo = "";

    public function __construct()
    {
        session_start();
        $this->pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');

    }

    public function setData($data = '')
    {
        if (array_key_exists('title', $data)) {
            $this->title = $data['title'];
        }
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('fullname', $data)) {
            $this->fullname = $data['fullname'];
        }
        if (array_key_exists('description', $data)) {
            $this->description = $data['description'];
        }
        if (array_key_exists('img', $data)) {
            $this->img= $data['img'];
        }
        if (array_key_exists('address', $data)) {
            $this->address= $data['address'];
        }
        if (array_key_exists('postid', $data)) {
            $this->postid = $data['postid'];
        }
        return $this;
    }//End of setData

//    public function store()
//    {
//        try {
//            $query = "INSERT INTO `settings` (`id`, `user_id`, `title`,`fullname`, `description`,`address`,`img`)
//                      VALUES (:id,:uid,:title,:fullname,:description,:address,:img)";
//            $stmnt = $this->pdo->prepare($query);
//            $stmnt->execute(
//                array(
//                    ':id' => null,
//                    ':uid' => $this->id,
//                    ':title' => $this->title,
//                    ':fullname' => $this->fullname,
//                    ':description' => $this->description,
//                    ':address' => $this->address,
//                    ':img' => $this->img,
//                )
//            );
//            if ($stmnt) {
//                $_SESSION['setting-message'] = "Successfully Data Stored";
//                header("location:add_setting.php?id=$this->id");
//            }
//        } catch (PDOException $e) {
//            echo 'Error' . $e->getMessage();
//        }
//    }//End of store
    public function show(){
        try{
            $query = "SELECT * FROM `settings` WHERE `user_id` = "."'".$this->id."'";
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute();
            $value = $stmnt->fetchAll();
            return $value;
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }//End of show
    public function settingshow(){
        try{
            $query = "SELECT * FROM `settings` WHERE `user_id` = "."'".$this->id."'";
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute();
            $value = $stmnt->fetch();
            return $value;
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }//End of show

    public function settingupdateshow(){
        try{
            $query = "SELECT * FROM `settings` WHERE `user_id` = "."'".$this->id."'";
//            echo $query;
//            die();
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute();
            $value = $stmnt->fetch();
            return $value;
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }//End of hobbiesshow
    public function update()
    {
        try {
//            echo $this->id.$this->title.$this->img.$this->fullname;
//            die();
            $query = 'UPDATE settings SET `title`=:title,`fullname`=:fullname,`description`=:description,
                      `address`=:address,`img`=:img WHERE user_id = :id';
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute(
                array(
                    ':id' => $this->id,
                    ':title' => $this->title,
                    ':fullname' => $this->fullname,
                    ':description' => $this->description,
                    ':address' => $this->address,
                    ':img' => $this->img,

                )
            );
            if ($stmnt) {
                $_SESSION['setting-message'] = "Successfully Data Updated";
                header("location:update_setting.php?id=$this->id");
            }
        }
        catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }//End of update
    public function delete()
    {
        try {
            $query = "DELETE FROM `portfolios` WHERE `portfolios`.`id` =". $this->id;
            $stmnt = $this->pdo->query($query);
            $stmnt->execute();
            if ($stmnt) {
                $_SESSION['message'] = "Successfully Data Deleted";
                header("location:../Portfolio/portfolio_view.php?id=$this->id");
            }
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }//End of Delete
}