<?php
namespace App\Portfolio;
use PDO;
class portfolio
{
    private $id = "";
    private $postid = "";
    private $mainid = "";
    private $title = "";
    private $description = "";
    private $img = "";
    private $category = "";
    private $pdo = "";

    public function __construct()
    {
        session_start();
        $this->pdo = new PDO('mysql:host=localhost;dbname=cvbank', 'root', '');

    }

    public function setData($data = '')
    {
        if (array_key_exists('title', $data)) {
            $this->title = $data['title'];
        }
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('description', $data)) {
            $this->description = $data['description'];
        }
        if (array_key_exists('img', $data)) {
            $this->img= $data['img'];
        }
        if (array_key_exists('category', $data)) {
            $this->category= $data['category'];
        }
        if (array_key_exists('postid', $data)) {
            $this->postid = $data['postid'];
        }
        if (array_key_exists('mainid', $data)) {
            $this->mainid = $data['mainid'];
        }
        return $this;
    }//End of setData

    public function store()
    {
        try {
            $query = "INSERT INTO `portfolios` (`id`, `user_id`, `title`, `description`,`img`,`category`)
                      VALUES (:id,:uid,:title,:description,:img,:category)";
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute(
                array(
                    ':id' => null,
                    ':uid' => $this->id,
                    ':title' => $this->title,
                    ':description' => $this->description,
                    ':img' => $this->img,
                    ':category' => $this->category,
                )
            );
            if ($stmnt) {
                $_SESSION['portfolio-message'] = "Successfully Data Stored";
                header("location:add_portfolio.php?id=$this->id");
            }
        } catch (PDOException $e) {
            echo 'Error' . $e->getMessage();
        }
    }//End of store
    public function show(){
        try{
            $query = "SELECT * FROM `portfolios` WHERE `user_id` = "."'".$this->id."'";
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute();
            $value = $stmnt->fetchAll();
            return $value;
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }//End of show

    public function portfolioshow(){
        try{
            $query = "SELECT * FROM `portfolios` WHERE `id` = "."'".$this->id."'";
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute();
            $value = $stmnt->fetch();
            return $value;
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }//End of hobbiesshow
    public function portfolioupdateshow(){
        try{
            $query = "SELECT * FROM `portfolios` WHERE `id` = "."'".$this->postid."'";
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute();
            $value = $stmnt->fetch();
            return $value;
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }//End of hobbiesshow
    public function update()
    {
        try {
            $query = 'UPDATE portfolios SET `id`=:id,`user_id`=:uid,`title`=:title,`description`=:description,
                      `img`=:img, `category`=:category WHERE id = :id';
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute(
                array(
                    ':id' => $this->postid,
                    ':uid' => $this->id,
                    ':title' => $this->title,
                    ':description' => $this->description,
                    ':img' => $this->img,
                    ':category' => $this->category,

                )
            );
            if ($stmnt) {
                $_SESSION['portfolio-message'] = "Successfully Data Updated";
                header("location:update_portfolio.php?id=$this->postid");
            }
        }
        catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }//End of update
    public function delete()
    {
        try {
            $query = "DELETE FROM `portfolios` WHERE `portfolios`.`id` =". $this->id;
            $stmnt = $this->pdo->query($query);
            $stmnt->execute();
            if ($stmnt) {
                $_SESSION['message'] = "Successfully Data Deleted";
                header("location:../Portfolio/portfolio_view.php?id=$this->mainid");
            }
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }//End of Delete
}