
<?php
include_once ("../../vendor/autoload.php");
use App\Blog\blog;
session_start();
$id=$_POST['id'];

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    if (empty($_POST['title'])) {
        $_SESSION['blog-message'] = "Please Enter Title.";
        header("location:add_blog.php?id=$id");
    }
    else if (empty($_POST['description'])) {
        $_SESSION['blog-message'] = "Please Enter Your Description.";
        header("location:add_blog.php?id=$id");
    }
    else if (empty($_POST['tags'])) {
        $_SESSION['blog-message'] = "Please Enter Your starting date.";
        header("location:add_blog.php?id=$id");
    }
    else if (empty($_POST['categories'])) {
        $_SESSION['blog-message'] = "Please Enter Your end date.";
        header("location:add_blog.php?id=$id");
    }
    else {
        $reg = new blog();
        $reg->setData($_POST)->store();
    }
}else{
    header("location:add_blog.php?id=$id");
}