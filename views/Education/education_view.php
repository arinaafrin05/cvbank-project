<?php
include_once ("../../vendor/autoload.php");
use App\education\education;
$obj = new education();
$obj->setData($_GET);
$value = $obj->show();
?>
<?php
if (!empty($_SESSION['user_info'])) {
?>
<?php include_once"../header.php"; ?>


<?php include_once("../Admin/side-menubar.php"); ?>



    <!-- Main content -->
    <div class="content-wrapper">

        <!-- Page header -->
        <div class="page-header">
            <div class="page-header-content">
                <div class="page-title">
                    <h4><i class="icon-arrow-left52 position-left"></i> <span
                            class="text-semibold">Home</span> - Dashboard</h4>
                </div>
            </div>

            <div class="breadcrumb-line">
                <ul class="breadcrumb">
                    <li><a href="../Admin/dashboard.php"><i class="icon-home2 position-left"></i> Home</a></li>
                    <li class="active">Dashboard</li>
                </ul>
            </div>
        </div>
        <!-- /page header -->


        <!-- Content area -->
        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-flat">
                        <div class="panel-heading">
                            <h2 class="panel-title">Education Table
                                <span class="label label-success position-right" style="font-size: 14px"><?php
                                    if (isset($_SESSION['message'])){
                                        echo $_SESSION['message'];
                                        unset($_SESSION['message']);
                                    } ?>
                                </span>
                            </h2>
                            <div class="heading-elements">
                                <span class="label label-primary heading-text"><a href="../Education/add_education.php?id=<?php echo $_SESSION['user_info']['id'];?>" style="color: black;font-size: 14px" >Add New Education</a></span>
                            </div>
                        </div>
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Title</th>
                                <th>Institute</th>
                                <th>Result</th>
                                <th>Passing Year</th>
                                <th>Subject</th>
                                <th>Board</th>
                                <th>Course Duration</th>
                                <th>EDIT/DELETE</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($value as $key => $item ){ ?>
                            <tr>
                                <td><?php echo $item['title']; ?></td>
                                <td><?php echo $item['institute']; ?></td>
                                <td><?php echo $item['result']; ?></td>
                                <td><?php echo $item['passing_year']; ?></td>
                                <td><?php echo $item['main_subject']; ?></td>
                                <td><?php echo $item['education_board']; ?></td>
                                <td><?php echo $item['course_duration']; ?></td>
                                <td>
                                    <a href="../Education/update_education.php?id=<?php echo $item['id']; ?>" class="btn btn-success">Edit</a>
                                    <a href="../Education/education_delete.php?id=<?php echo $item['id']; ?> && mainid=<?php echo $_SESSION['user_info']['id'];?>" onclick="return confirm('Do You Want To Delete?')" class="btn btn-danger">DELETE</a>
                                </td>
                            </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    </div>
            </div>
        </div>
                <!-- /main charts -->


    <?php
      include_once("../footer.php");
    ?>

    		<?php
	} else{
		$_SESSION['fail']= "You are not authorized!";
		header('location:../../../index.php');
	}

?>