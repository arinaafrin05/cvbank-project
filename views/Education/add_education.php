<?php
session_start();
if (!empty($_SESSION['user_info'])) { ?>
<?php include_once"../header.php"; ?>


<?php include_once("../Admin/side-menubar.php"); ?>



    <!-- Main content -->
    <div class="content-wrapper">

        <!-- Page header -->
        <div class="page-header">
            <div class="page-header-content">
                <div class="page-title">
                    <h4><i class="icon-arrow-left52 position-left"></i> <span
                            class="text-semibold">Home</span> - Dashboard</h4>
                </div>
            </div>

            <div class="breadcrumb-line">
                <ul class="breadcrumb">
                    <li><a href="../Admin/dashboard.php"><i class="icon-home2 position-left"></i> Home</a></li>
                    <li class="active">Dashboard</li>
                </ul>
            </div>
        </div>
        <!-- /page header -->


        <!-- Content area -->
        <div class="content">
            <div class="row">
                <div class="col-md-8">

                    <!-- Basic layout-->
                        <div class="panel panel-flat">
                            <div class="panel-heading">
                                <h2 class="panel-title">Academic Qualification
                                    <span class="label label-success position-right" style="font-size: 14px">
                                        <?php
                                            if (isset($_SESSION['message'])){
                                                echo $_SESSION['message'];
                                                unset($_SESSION['message']);
                                            }
                                        ?>
                                    </span>
                                </h2>
                                <div class="heading-elements">
                                    <span class="label label-primary heading-text"><a href="../Education/education_view.php?id=<?php echo $_SESSION['user_info']['id'];?>" style="color: black;font-size: 14px" >My Educations</a></span>
                                </div>
                            </div>
                            <form action="education_store.php" class="form-horizontal" method="post">
                            <div class="panel-body">
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Course Name :</label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" placeholder="Your Course name" name="title"required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Institute Name :</label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" placeholder="Your institute name" name="institute"required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Result :</label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" name="result" placeholder="Your result"required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Passing Year :</label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" name="passing_year" placeholder="Passing Year"required/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Subject :</label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" name="main_subject" placeholder="Your subject"required/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Board :</label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" name="education_board" placeholder="Your Board name"required/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Course Duration :</label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" name="course_duration" placeholder="Your course duration"required/>
                                    </div>
                                </div>

                                <div class="text-right">
                                    <button type="submit" class="btn btn-primary">ADD<i class="icon-arrow-right14 position-right"></i></button>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="id" value="<?php echo $_SESSION['user_info']['id']; ?>">
                    </form>
                    <!-- /basic layout -->
                </div>
            </div>
        </div>
                <!-- /main charts -->
    <?php
    include_once("../footer.php");
  ?>
                <?php
                } else{
                    $_SESSION['fail']= "You are not authorized!";
                    header('location:../../../index.php');
                }

                ?>
