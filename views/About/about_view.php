<?php
include_once ("../../vendor/autoload.php");
use App\about\about;
$obj = new about();
$obj->setData($_GET);
$value = $obj->show();
?>
<?php
if (!empty($_SESSION['user_info'])) {
?>
<?php include_once"../header.php"; ?>


<?php include_once("../Admin/side-menubar.php"); ?>



    <!-- Main content -->
    <div class="content-wrapper">

        <!-- Page header -->
        <div class="page-header">
            <div class="page-header-content">
                <div class="page-title">
                    <h4><i class="icon-arrow-left52 position-left"></i> <span
                            class="text-semibold">Home</span> - Dashboard</h4>
                </div>
            </div>

            <div class="breadcrumb-line">
                <ul class="breadcrumb">
                    <li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
                    <li class="active">Dashboard</li>
                </ul>
            </div>
        </div>
        <!-- /page header -->


        <!-- Content area -->
        <div class="content">
            <div class="row">
                <div class="col-md-12">

                    <!-- Basic layout-->

                    <h2>About Table</h2>
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Title</th>
                                <th>Phone No</th>
                                <th>Description</th>
                                <th>EDIT/DELETE</th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><?php echo $value['title'];?></td>
                                    <td><?php echo $value['phone'];?></td>
                                    <td><?php echo $value['bio'];?></td>
                                    <td>
                                        <a href="../About/update_about.php?id=<?php echo $_SESSION['user_info']['id']; ?>" class="btn btn-success">Edit</a>
                                        <a href="" class="btn btn-danger">DELETE</a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- /basic layout -->
                </div>
                <!-- /main charts -->
            </div>
            <!-- /content area -->

        </div>
    </div>
    <!-- /main content -->

<?php
  include_once("../footer.php");
?>
    <?php
	} else{
		$_SESSION['fail']= "You are not authorized!";
		header('location:../../../index.php');
	}

?>