<?php
include_once ("../../vendor/autoload.php");
use App\Experience\experience;
//print_r($_POST);
//die();
session_start();
$id=$_POST['id'];

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    if (empty($_POST['designation'])) {
        $_SESSION['experience-message'] = "Please Enter Title.";
        header("location:add_experience.php?id=$id");
    } else if (empty($_POST['company_name'])) {
        $_SESSION['experience-message'] = "Please Enter Your Description.";
        header("location:add_experience.php?id=$id");
    } else if (empty($_POST['start_date'])) {
        $_SESSION['experience-message'] = "Please Enter Your starting date.";
        header("location:add_experience.php?id=$id");
    } else if (empty($_POST['end_date'])) {
        $_SESSION['experience-message'] = "Please Enter Your end date.";
        header("location:add_experience.php?id=$id");
    } else if (empty($_POST['company_location'])) {
        $_SESSION['experience-message'] = "Please Enter Your company location.";
        header("location:add_experience.php?id=$id");
    } else {
        $reg = new experience();
        $reg->setData($_POST)->store();
    }
}