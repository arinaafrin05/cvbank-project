<?php
session_start();
if (!empty($_SESSION['user_info'])) {
?>
<?php include_once"../header.php"; ?>


<?php include_once("../Admin/side-menubar.php"); ?>



    <!-- Main content -->
    <div class="content-wrapper">

        <!-- Page header -->
        <div class="page-header">
            <div class="page-header-content">
                <div class="page-title">
                    <h4><i class="icon-arrow-left52 position-left"></i> <span
                            class="text-semibold">Home</span> - Dashboard</h4>
                </div>
            </div>

            <div class="breadcrumb-line">
                <ul class="breadcrumb">
                    <li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
                    <li class="active">Dashboard</li>
                </ul>
            </div>
        </div>
        <!-- /page header -->


        <!-- Content area -->
        <div class="content">
            <div class="row">
                <div class="col-md-8">

                    <!-- Basic layout-->
                        <div class="panel panel-flat">
                            <div class="panel-heading">
                                <h2 class="panel-title">Add New
                                    <span class="label label-success position-right" style="font-size: 14px"><?php
                                        if (isset($_SESSION['experience-message'])){
                                            echo $_SESSION['experience-message'];
                                            unset($_SESSION['experience-message']);
                                        } ?>
                                    </span>
                                </h2>
                                <div class="heading-elements">
                                    <span class="label label-primary heading-text"><a href="../Experience/experience_view.php?id=<?php echo $_SESSION['user_info']['id'];?>" style="color: black;font-size: 14px" >My Experiences</a></span>
                                </div>
                            </div>
                            <form action="experience_store.php" method="post" class="form-horizontal">
                            <div class="panel-body">
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Designation:</label>
                                    <div class="col-lg-9">
                                        <input type="text" name="designation" class="form-control" placeholder="Write your designation ">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Company Name:</label>
                                    <div class="col-lg-9">
                                        <input type="text" name="company_name" class="form-control" placeholder="Your institute name">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Start Date:</label>
                                    <div class="col-lg-9">
                                        <input type="date" class="form-control" name="start_date" placeholder="Year"/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">End Year:</label>
                                    <div class="col-lg-9">
                                        <input type="date" class="form-control" name="end_date" placeholder="Year"/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Company Location:</label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" name="company_location" placeholder="Your company Location"/>
                                    </div>
                                </div>

                                <div class="text-right">
                                    <button type="submit" class="btn btn-primary">ADD<i class="icon-arrow-right14 position-right"></i></button>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="id" value="<?php echo $_SESSION['user_info']['id']; ?>">
                    </form>
                    <!-- /basic layout -->
                </div>
                <!-- /main charts -->

<?php
include_once("../footer.php");
?>
        <?php
           } else{
               $_SESSION['fail']= "You are not authorized!";
                    header('location:../../../index.php');
              }

        ?>
