<?php
include_once ("../../vendor/autoload.php");
use App\login\login;
$obj = new login();
$obj->setData($_GET);
$value = $obj->adminupdateshow();
?>
    <?php
        if (!empty($_SESSION['user_info'])) {
    ?>


    <?php
    include ("../header.php");
    ?>
    <?php
    include "../Admin/side-menubar.php";
    ?>

    <!-- Main content -->
    <div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i> <span
                        class="text-semibold">Home</span> - Dashboard</h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="../Admin/dashboard.php"><i class="icon-home2 position-left"></i> Home</a></li>
                <li class="active">Dashboard</li>
            </ul>
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">
        <!-- Centered forms -->
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <h5 class="panel-title">Update Admin Data
                                <span class="label label-success position-right" style="font-size: 14px" ><?php
                                    if (isset($_SESSION['update-mess'])){
                                        echo $_SESSION['update-mess'];
                                        unset($_SESSION['update-mess']);
                                    } ?>
                                    </span>
                            </h5>
                            <div class="heading-elements">
                                <span class="label label-primary heading-text"><a href="../adminpanel/adminlist.php?id=<?php echo $_SESSION['user_info']['id'];?>" style="color: black;font-size: 14px" >All Admins</a></span>
                                <span class="label label-primary heading-text" style="margin-right: 80px;"><a href="../adminpanel/add_admin.php?id=<?php echo $_SESSION['user_info']['id'];?>" style="color: black;font-size: 14px" >Add New Admin</a></span>
                            </div>
                        </div>
                    </div>
                </div>

                <form action="admin_update.php" method="post">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-1">

                            <div class="form-group">
                                <label>Enter your name:</label>
                                <input type="text" class="form-control" name="username" value="<?php echo $value['username'];?>" placeholder="User Name">
                            </div>

                            <div class="form-group">
                                <label>Enter your Email:</label>
                                <input type="text" class="form-control" name="email" value="<?php echo $value['email'];?>" placeholder="Email Address">
                            </div>

                            <div class="form-group">
                                <label>Enter your password:</label>
                                <input type="password" class="form-control" name="password" value="<?php echo $value['password'];?>" placeholder="Your strong password">
                            </div>

                            <div class="text-right">
                                <button type="submit" class="btn btn-primary">Update <i class="icon-arrow-right14 position-right"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <input type="hidden" name="id" value="<?php echo $value['id'];?>"/>
        </form>
    </div>
    <!-- /main charts -->

    <?php
    include_once("../footer.php");
    ?>
    <?php
} else{
    $_SESSION['fail']= "You are not authorized!";
    header('location:../../index.php');
}

?>