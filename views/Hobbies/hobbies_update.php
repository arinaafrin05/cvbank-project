<?php
include_once ("../../vendor/autoload.php");
use App\Hobbies\hobbies;
session_start();
$id=$_POST['id'];

if ($_SERVER['REQUEST_METHOD'] == 'POST'){

    $imgFile = $_FILES['user_image']['name'];
    $tmp_dir = $_FILES['user_image']['tmp_name'];
    $imgSize = $_FILES['user_image']['size'];
    if($imgFile)
    {
        $upload_dir = '../../assets/upload_image/';
        $imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION)); // get image extension
        $valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
        $userpic = rand(1000,1000000).'hobbies'.".".$imgExt;
        if(in_array($imgExt, $valid_extensions))
        {
            if($imgSize < 5000000)
            {
                $obj = new hobbies();
                $obj->setData($_POST['postid']);
                $value = $obj->hobbiesupdateshow();
                unlink($upload_dir.$value['img']);
                move_uploaded_file($tmp_dir,$upload_dir.$userpic);
                $_POST['img'] = $userpic;
                $reg = new hobbies();
                $reg->setData($_POST)->update();
            }
            else
            {
                $_SESSION['hobbies-message'] = "Sorry, your file is too large.";
                header("location:update_hobbies.php?id=$id");
            }
        }
        else
        {
            $_SESSION['hobbies-message'] = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            header("location:update_hobbies.php?id=$id");
        }
    }
    else
    {
        $postid = $_POST['postid'];
        $obj = new hobbies();
        $obj->setData($_POST);
        $value = $obj->hobbiesupdateshow();
        $userpic = $value['img'];
        $_POST['img'] = $userpic;
        $reg = new hobbies();
        $reg->setData($_POST)->update();
    }

}else {
    header("location:add_hobbies.php?id=$id");
}