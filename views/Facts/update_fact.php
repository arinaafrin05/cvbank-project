<?php
include_once ("../../vendor/autoload.php");
use App\Facts\fact;
$obj = new fact();
$obj->setData($_GET);
$value = $obj->factshow();
?>
<?php
if (!empty($_SESSION['user_info'])) {
    ?>
    <?php include_once"../header.php"; ?>


    <?php include_once("../Admin/side-menubar.php"); ?>



    <!-- Main content -->
    <div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i> <span
                        class="text-semibold">Home</span> - Dashboard</h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
                <li class="active">Dashboard</li>
            </ul>
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">
    <div class="row">
    <div class="col-md-8">

        <!-- Basic layout-->
        <form action="fact_update.php" method="post" class="form-horizontal" enctype="multipart/form-data">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h2 class="panel-title">Update Your Data
                        <span class="label label-success position-right" style="font-size: 14px"><?php
                            if (isset($_SESSION['update-mess'])){
                                echo $_SESSION['update-mess'];
                                unset($_SESSION['update-mess']);
                            } ?></span>
                    </h2>
                    <div class="heading-elements">
                        <span class="label label-primary heading-text"><a href="../Facts/add_fact.php?id=<?php echo $_SESSION['user_info']['id'];?>" style="color: black;font-size: 14px" >Add New Fact</a></span>
                        <span class="label label-primary heading-text"><a href="../Facts/fact_view.php?id=<?php echo $_SESSION['user_info']['id'];?>" style="color: black;font-size: 14px" >My Facts</a></span>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="form-group">
                        <label class="col-lg-3 control-label">Title:</label>
                        <div class="col-lg-9">
                            <input type="text" name="title" class="form-control" value="<?php echo $value['title'];?>" placeholder="Write your title ">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label">Item:</label>
                        <div class="col-lg-9">
                            <input type="text" name="no_of_items" value="<?php echo $value['no_of_items'];?>" class="form-control" placeholder="no of item">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label">Uploaded Image : </label>
                        <div class="col-lg-9">
                            <div class="thumbnail">
                                <div class="thumb">
                                    <img style="width: 40px;height:40px" src="../../assets/upload_image/<?php echo $value['img']; ?>">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label">Upload Image:</label>
                        <div class="col-lg-9">
                            <input type="file" name="user_image" accept="image/*">
                        </div>
                    </div>

                    <div class="text-right">
                        <button type="submit" class="btn btn-primary">Update<i class="icon-arrow-right14 position-right"></i></button>
                    </div>
                </div>
            </div>
            <input type="hidden" name="id" value="<?php echo $_SESSION['user_info']['id']; ?>">
            <input type="hidden" name="postid" value="<?php echo $value['id']; ?>">
        </form>
        <!-- /basic layout -->
    </div>
    <!-- /main charts -->

    <?php
    include_once("../footer.php");
    ?>

    <?php
} else{
    $_SESSION['fail']= "You are not authorized!";
    header('location:../../../index.php');
}

?>