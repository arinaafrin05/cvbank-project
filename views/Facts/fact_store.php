<?php
include_once ("../../vendor/autoload.php");
use App\Facts\fact;
//print_r($_POST);
//die();
session_start();
$id=$_POST['id'];
//$imgFile=$_POST['user_image'];

if ($_SERVER['REQUEST_METHOD'] == 'POST'){

    $imgFile = $_FILES['user_image']['name'];
    $tmp_dir = $_FILES['user_image']['tmp_name'];
    $imgSize = $_FILES['user_image']['size'];


    if(empty($_POST['title'])){
        $_SESSION['fact-message'] = "Please Enter Title.";
        header("location:add_fact.php?id=$id");
    }
    else if(empty($_POST['no_of_items'])){
        $_SESSION['fact-message'] = "Please Enter Your Description.";
        header("location:add_fact.php?id=$id");
    }
    else if(empty($imgFile)){
        $_SESSION['fact-message'] = "Please Select Image File.";
        header("location:add_fact.php?id=$id");
    }
    else
    {
        $upload_dir = '../../assets/upload_image/';
        $imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION));
        $valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
        $userpic = rand(1000,1000000).'fact'.".".$imgExt;
        if(in_array($imgExt, $valid_extensions)){
            if($imgSize < 5000000)    {
                move_uploaded_file($tmp_dir,$upload_dir.$userpic);
                $_POST['img'] = $userpic;
                $reg = new fact();
                $reg->setData($_POST)->store();
            }
            else{
                $_SESSION['fact-message'] = "Sorry, your file is too large.";
                header("location:add_fact.php?id=$id");
            }
        }
        else{
            $_SESSION['fact-message'] = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            header("location:add_fact.php?id=$id");
        }
    }
}else {
    header("location:add_fact.php?id=$id");
}