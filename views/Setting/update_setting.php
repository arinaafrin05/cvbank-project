<?php
include_once ("../../vendor/autoload.php");
use App\setting\setting;
$obj = new setting();
$obj->setData($_GET);
$value = $obj->settingshow();
//print_r($value);
//die();
?>
<?php
if (!empty($_SESSION['user_info'])) {
    ?>
<?php include_once"../header.php"; ?>

<?php include_once("../Admin/side-menubar.php"); ?>

    <!-- Main content -->
    <div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i> <span
                        class="text-semibold">Home</span> - Dashboard</h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
                <li class="active">Dashboard</li>
            </ul>
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">
    <div class="row">
    <div class="col-md-8">

        <!-- Basic layout-->
        <form action="setting_update.php" method="post" class="form-horizontal" enctype="multipart/form-data">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title">Update Your Data
                        <span class="label label-success position-right" style="font-size: 14px"><?php
                            if (isset($_SESSION['setting-message'])){
                                echo $_SESSION['setting-message'];
                                unset($_SESSION['setting-message']);
                            } ?></span>
                    </h5>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                            <li><a data-action="reload"></a></li>
                            <li><a data-action="close"></a></li>
                        </ul>
                    </div>
                </div>

                <div class="panel-body">

                    <div class="form-group">
                        <label class="col-lg-3 control-label">Title:</label>
                        <div class="col-lg-9">
                            <input type="text" name="title" value="<?php echo $value['title'];?>" class="form-control" placeholder="Write your title">
                        </div>
                    </div>

                    <div class="form-group">
                            <label class="col-lg-3 control-label">Full Name:</label>
                        <div class="col-lg-9">
                            <input type="text" name="fullname" value="<?php echo $value['fullname'];?>" class="form-control" placeholder="Write your full name">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label">Your Description:</label>
                        <div class="col-lg-9">
                            <textarea rows="5" cols="5" name="description" class="form-control" placeholder="Enter your message here"><?php echo $value['description'];?></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label">Address:</label>
                        <div class="col-lg-9">
                            <input type="text" name="address" value="<?php echo $value['address'];?>" class="form-control" placeholder="Write your Address">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label">Uploaded Image : </label>
                        <div class="col-lg-9">
                            <div class="thumbnail">
                                <div class="thumb">
                                    <img style="width: 40px;height:40px" src="../../assets/upload_image/<?php echo $value['img']; ?>">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label">Upload Image:</label>
                        <div class="col-lg-9">
                            <input type="file" name="user_image" accept="image/*">
                        </div>
                    </div>

                    <div class="text-right">
                        <button type="submit" class="btn btn-primary">Update<i class="icon-arrow-right14 position-right"></i></button>
                    </div>
                </div>
            </div>
            <input type="hidden" name="id" value="<?php echo $_SESSION['user_info']['id']; ?>">
        </form>
        <!-- /basic layout -->
    </div>
    <!-- /main charts -->

<?php
include_once("../footer.php");
?>
    <?php
} else{
    $_SESSION['fail']= "You are not authorized!";
    header('location:../../../index.php');
}

?>