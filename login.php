<?php
include_once ("vendor/autoload.php");
use App\login\login;

if ($_SERVER['REQUEST_METHOD'] == 'POST'){
    $login = new login();
    $login->setData($_POST)->login();
}else {
    header("location:index.php");
}
